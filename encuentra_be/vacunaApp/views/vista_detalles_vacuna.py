from rest_framework import status,views
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from vacunaApp.serializers.detalles_vacunaSerializer import DetalleVacunaSerializer

class DetalleCreateView(views.APIView):

    def post(self, request, *args, **kwargs):
        serializer = DetalleVacunaSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(status=status.HTTP_201_CREATED)