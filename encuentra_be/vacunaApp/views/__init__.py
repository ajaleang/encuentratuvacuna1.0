from .vista_creacion_puntovacunacion import UserCreateView
from .vista_detalle_puntovacunacion import UserDetailView
from .vista_detalles_vacuna import DetalleCreateView
from .vista_vacuna import VacunaCreateView
