from django.apps import AppConfig


class VacunaappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'vacunaApp'
