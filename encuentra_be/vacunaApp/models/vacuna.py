from django.db import models
from .puntovacunacion import User

class Vacuna(models.Model):
    idVacuna = models.AutoField(primary_key=True)
    nombreVacuna = models.CharField('Nombre Vacuna', max_length = 30)