from django.db import models
from .puntovacunacion import User
from .vacuna import Vacuna

class Detalles_vacuna(models.Model):
    id = models.AutoField(primary_key=True)
    idPunto1 = models.ForeignKey(User, related_name='idPunto1', on_delete=models.CASCADE)
    idVacuna1 = models.ForeignKey(Vacuna, related_name='idVacuna1', on_delete=models.CASCADE)
    cantidad_dosis1=models.IntegerField(default=0)
    cantidad_dosis2=models.IntegerField(default=0)