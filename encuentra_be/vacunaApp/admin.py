from django.contrib import admin
from .models.puntovacunacion import User
from .models.vacuna import Vacuna
from .models.detalles_vacuna import Detalles_vacuna

admin.site.register(User)
admin.site.register(Vacuna)
admin.site.register(Detalles_vacuna)