from django.db.models import fields
from rest_framework import serializers
from vacunaApp.models.detalles_vacuna import Detalles_vacuna


class DetalleVacunaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Detalles_vacuna
        #fields = ['idPunto', 'idVacuna', 'cantidad_dosis1', 'cantidad_dosis2']
        fields = ['id', 'idPunto1', 'idVacuna1', 'cantidad_dosis1', 'cantidad_dosis2']
    def create(self, validated_data):
        detalleInstance = Detalles_vacuna.objects.create(**validated_data)
        return detalleInstance
    
    def to_representation(self, obj):
        detalle = Detalles_vacuna.objects.get(id=obj.id)
        
        return {
            'id':detalle.id,
            'idPunto1': detalle.idPunto1,
            'idVacuna1': detalle.idVacuna1,
            'cantidad_dosis1': detalle.cantidad_dosis1,
            'cantidad_dosis2': detalle.cantidad_dosis2
                       
        }