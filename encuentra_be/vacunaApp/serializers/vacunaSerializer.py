from django.db.models import fields
from rest_framework import serializers
from vacunaApp.models.vacuna import Vacuna

class VacunaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vacuna
        fields = ['idVacuna', 'nombreVacuna']
    
    def create(self, validated_data):
        vacunaInstance = Vacuna.objects.create(**validated_data)
        return vacunaInstance
    
    def to_representation(self, obj):
        vacuna = Vacuna.objects.get(id=obj.idVacuna)
        return {
            'idVacuna': vacuna.idVacuna,
            'nombreVacuna': vacuna.nombreVacuna            
        }