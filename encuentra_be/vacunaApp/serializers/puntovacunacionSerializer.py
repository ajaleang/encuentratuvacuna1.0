from django.db.models import fields
from rest_framework import serializers
from vacunaApp.models.puntovacunacion import User

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['idPunto','username','password','nombrePunto','direccion','departamento', 'ciudad']
    
    def create(self, validated_data):
        userInstance = User.objects.create(**validated_data)
        return userInstance
    
    def to_representation(self, obj):
        user = User.objects.get(id=obj.idPunto)
        return {
            'idPunto': user.idPunto,
            'username': user.username,
            'password':user.password,
            'nombrePunto': user.nombrePunto,
            'direccion': user.direccion,
            'departamento': user.departamento,
            'ciudad': user.ciudad
        }